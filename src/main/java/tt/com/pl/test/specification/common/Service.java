package tt.com.pl.test.specification.common;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface Service<K, E extends Entity<K>> {

    JpaRepository<E, K> getRepository();

    default Optional<E> findById(K key) {
        return getRepository().findById(key);
    }

}
