package tt.com.pl.test.specification.common.specification;

import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.stream.Stream;

import static tt.com.pl.test.specification.common.specification.CriteriaUtils.getPath;

class InProcessor implements Processor<In> {

    @Setter
    In annotation;

    @Override
    @SneakyThrows
    public <E, P> Specification<E> process(Specification<E> specification, Field field, P params) {
        Object value = field.get(params);

        if (Objects.nonNull(value)) {
            Specification<E> eSpecification = (eventRoot, criteriaQuery, criteriaBuilder) ->
                    getSpecification(value, eventRoot, criteriaBuilder);

            specification = eSpecification.and(specification);
        }


        return specification;
    }

    private <E> CriteriaBuilder.In<Object> getSpecification(Object value, javax.persistence.criteria.Root<E> eventRoot, CriteriaBuilder criteriaBuilder) {
        CriteriaBuilder.In<Object> inCriteria = criteriaBuilder.in(getPath(annotation.field(), eventRoot));

        Stream.of(value)
                .forEach(inCriteria::value);

        return inCriteria;
    }


}

