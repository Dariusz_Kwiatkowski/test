package tt.com.pl.test.specification.common;

public interface Entity<K> {
    K getId();
}
