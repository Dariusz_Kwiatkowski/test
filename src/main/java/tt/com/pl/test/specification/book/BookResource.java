package tt.com.pl.test.specification.book;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tt.com.pl.test.specification.common.CrudResource;
import tt.com.pl.test.specification.common.SearchService;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/book")
public class BookResource implements CrudResource<Long, BookEntity, BookParams> {

    @Getter
    private final BookService bookService;


    @Override
    public SearchService<Long, BookEntity, BookParams> getSearchService() {
        return bookService;
    }


}
