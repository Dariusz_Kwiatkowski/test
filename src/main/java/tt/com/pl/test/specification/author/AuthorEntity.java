package tt.com.pl.test.specification.author;

import lombok.Getter;
import lombok.Setter;
import tt.com.pl.test.specification.common.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;


@Table(name = "author")
@Entity
@Getter
@Setter
public class AuthorEntity extends AbstractEntity<Long> {

    private String name;
    private String surname;


}
