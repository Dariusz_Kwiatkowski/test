package tt.com.pl.test.specification.book;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tt.com.pl.test.specification.author.AuthorEntity;
import tt.com.pl.test.specification.common.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = "book")
@Getter
@Setter
@ToString
@NamedEntityGraph(name = "bookWithAuthor", attributeNodes = @NamedAttributeNode("author"))
public class BookEntity extends AbstractEntity<Long> {

//    private String author;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private AuthorEntity author;
}
