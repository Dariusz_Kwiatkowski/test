package tt.com.pl.test.specification.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import tt.com.pl.test.specification.common.specification.SpecificationBuilder;

import java.util.List;
import java.util.Optional;

public interface SearchService<K, E extends Entity<K>, P> {

    JpaSpecificationExecutor<E> getRepository();


    default Specification<E> getSpecification(P params) {
        return SpecificationBuilder.<K, E, P>build(params);
    }

    default Optional<E> searchById(K id) {
        return getRepository()
                .findOne((eventRoot, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(eventRoot.get("id"), id));


    }

    default List<E> search(P params) {
        return getRepository().findAll(getSpecification(params));
    }


    default Page<E> search(P params, Pageable pageable) {
        return getRepository().findAll(getSpecification(params), pageable);
    }
}
