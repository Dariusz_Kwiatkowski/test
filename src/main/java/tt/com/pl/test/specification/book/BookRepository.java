package tt.com.pl.test.specification.book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long>, JpaSpecificationExecutor<BookEntity> {

    @EntityGraph("bookWithAuthor")
    List<BookEntity> findByNameContaining(String name);

    @Override
    @EntityGraph("bookWithAuthor")
    List<BookEntity> findAll(Specification<BookEntity> specification);

    @Override
    @EntityGraph("bookWithAuthor")
    Page<BookEntity> findAll(Specification<BookEntity> specification, Pageable pageable);
}
