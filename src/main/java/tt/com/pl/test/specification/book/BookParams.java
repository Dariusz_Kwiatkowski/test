package tt.com.pl.test.specification.book;

import lombok.Getter;
import lombok.Setter;
import tt.com.pl.test.specification.common.specification.Equals;
import tt.com.pl.test.specification.common.specification.In;
import tt.com.pl.test.specification.common.specification.Like;

import java.util.List;

@Getter
@Setter
public class BookParams {


//    @Like(field = "author")
//    private String author;

    @Like(field = "name")
    private String name;

    @Equals(field = "id")
    private Long id;

    @In(field = "id")
    private List<Long> ids;


    @Like(field = "author.surname")
    private String authorSurname;

    @Like(field = "author.name")
    private String authorName;
}
