package tt.com.pl.test.specification.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface SearchResource<K, E extends Entity<K>, P> {

    SearchService<K, E, P> getSearchService();

    @GetMapping("/{entityId}")
    default E searchById(@PathVariable(value = "entityId") K entityId) {
        return getSearchService().searchById(entityId).orElseThrow();
    }

    @GetMapping
    default List<E> search(@RequestBody P params) {
        return getSearchService().search(params);
    }

    @GetMapping(params = "page")
    default Page<E> search(@RequestBody P params, Pageable pageable) {
        return getSearchService().search(params, pageable);
    }

}
