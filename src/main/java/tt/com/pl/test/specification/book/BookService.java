package tt.com.pl.test.specification.book;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tt.com.pl.test.specification.common.SearchService;
import tt.com.pl.test.specification.common.Service;

@org.springframework.stereotype.Service
@RequiredArgsConstructor
public class BookService implements Service<Long, BookEntity>, SearchService<Long, BookEntity, BookParams> {


    @Getter
    private final BookRepository repository;


}
