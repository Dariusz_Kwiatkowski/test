package tt.com.pl.test.conditional;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/conditional")
@RequiredArgsConstructor
public class ConditionalController {

    private final TestProperty testProperty;

    private final OsTestComponent osTestComponent;

    @GetMapping(value = "/test")
    public String testConditional() {
        return testProperty.test();
    }

    @GetMapping(value = "/osTest")
    public String testOs() {
        return osTestComponent.test();
    }


}
