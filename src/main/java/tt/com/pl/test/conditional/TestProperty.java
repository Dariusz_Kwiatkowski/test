package tt.com.pl.test.conditional;


import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(
        value = "test.conditional.exists",
        havingValue = "true",
        matchIfMissing = true)
public class TestProperty {

    public String test() {
        return "TestProperty exists";
    }
}
