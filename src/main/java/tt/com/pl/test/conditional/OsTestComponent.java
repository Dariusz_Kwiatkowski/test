package tt.com.pl.test.conditional;

import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;

@Component
@Conditional(WindowsOsCondition.class)
public class OsTestComponent {

    public String test() {
        return "OsTestComponent exists";
    }
}
