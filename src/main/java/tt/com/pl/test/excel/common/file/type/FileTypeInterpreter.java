package tt.com.pl.test.excel.common.file.type;


import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class FileTypeInterpreter {

    public enum FileType {
        XLSX, XLS, CSV, OTHER;
    }

    public FileType getFileType(final String fileName) {
        return Arrays.stream(FileType.values())
                .filter(x -> fileName.toUpperCase().endsWith(x.toString()))
                .findFirst()
                .orElse(FileType.OTHER);
    }
}
