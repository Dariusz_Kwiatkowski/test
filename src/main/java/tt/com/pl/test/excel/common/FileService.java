package tt.com.pl.test.excel.common;

import tt.com.pl.test.excel.common.file.type.FileTypeInterpreter;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class FileService<T> {

    private final FileTypeInterpreter fileTypeInterpreter;
    private Map<FileTypeInterpreter.FileType, FileReader<T>> fileTypeStrategy = new HashMap<>();


    public FileService(List<FileReader> fileReaders,
                       FileTypeInterpreter fileTypeInterpreter) {
        this.fileTypeInterpreter = fileTypeInterpreter;

        fileReaders.stream()
                .forEach(x -> fileTypeStrategy.put(x.getFileType(), x));

    }

    abstract protected Class<T> getClazz();

    List<T> readFile(String filePath, int sheetNumber) {
        FileReader<T> fileReader = getExcelFileReader(filePath);
        return fileReader.readFile(filePath, sheetNumber, getClazz());
    }

    List<T> readFile(InputStream inputStream, int sheetNumber, String fileName) {
        FileReader<T> fileReader = getExcelFileReader(fileName);
        return fileReader.readFile(inputStream, sheetNumber, getClazz());
    }

    private FileReader<T> getExcelFileReader(String file) {
        FileTypeInterpreter.FileType fileType = fileTypeInterpreter.getFileType(file);
        return fileTypeStrategy.get(fileType);
    }
}
