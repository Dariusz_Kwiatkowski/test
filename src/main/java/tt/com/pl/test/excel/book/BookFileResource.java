package tt.com.pl.test.excel.book;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tt.com.pl.test.excel.common.FileResource;

@RestController
@RequestMapping("api/file")
@RequiredArgsConstructor
public class BookFileResource implements FileResource<BookFile> {

    @Getter
    private final BookFileService fileService;


}
