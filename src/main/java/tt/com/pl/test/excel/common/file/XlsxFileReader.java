package tt.com.pl.test.excel.common.file;

import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import tt.com.pl.test.excel.common.file.type.FileTypeInterpreter;

import java.io.InputStream;

@Component
public class XlsxFileReader<T> extends ExcelFileReader<T> {


    @Override
    @SneakyThrows
    protected Workbook getWorkBook(InputStream inputStream) {
        return new XSSFWorkbook(inputStream);
    }

    @Override
    public FileTypeInterpreter.FileType getFileType() {
        return FileTypeInterpreter.FileType.XLSX;
    }
}
