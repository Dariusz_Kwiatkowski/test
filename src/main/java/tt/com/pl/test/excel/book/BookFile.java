package tt.com.pl.test.excel.book;

import lombok.Getter;
import lombok.Setter;
import tt.com.pl.test.excel.common.SheetCell;


@Getter
@Setter
public class BookFile {

    @SheetCell(name = "NAME")
    String name;

    @SheetCell(name = "AUTHOR_NAME")
    String authorName;

    @SheetCell(name = "AUTHOR_SURNAME")
    String authorSurname;
}

