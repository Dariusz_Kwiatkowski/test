package tt.com.pl.test.excel.book;

import org.springframework.stereotype.Service;
import tt.com.pl.test.excel.common.FileReader;
import tt.com.pl.test.excel.common.FileService;
import tt.com.pl.test.excel.common.file.type.FileTypeInterpreter;

import java.util.List;

@Service
public class BookFileService extends FileService<BookFile> {


    public BookFileService(List<FileReader> fileReaders,
                           FileTypeInterpreter fileTypeInterpreter) {
        super(fileReaders, fileTypeInterpreter);
    }

    @Override
    protected Class<BookFile> getClazz() {
        return BookFile.class;
    }
}
