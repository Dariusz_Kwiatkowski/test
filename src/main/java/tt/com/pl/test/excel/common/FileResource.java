package tt.com.pl.test.excel.common;

import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public interface FileResource<T> {

    FileService<T> getFileService();

    @GetMapping(value = "/readFromPath")
    default List<T> readFileFromPath(@RequestBody FileInfo fileInfo) {
        return this.getFileService().readFile(fileInfo.getFilePath(), fileInfo.getSheetNumber());
    }

    @GetMapping(value = "/readFile")
    @SneakyThrows
    default List<T> readFile(@RequestParam("file") MultipartFile file, @RequestParam Integer sheetNumber) {
        //return this.getFileService().readFile(file.getInputStream(), sheetNumber);
        return this.getFileService().readFile(file.getInputStream(), sheetNumber, file.getOriginalFilename());
    }
}
