package tt.com.pl.test.excel.common;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import tt.com.pl.test.excel.common.file.type.FileTypeInterpreter;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface FileReader<T> {

    FileTypeInterpreter.FileType getFileType();

    List<T> readFile(String filePath, int sheetNumber, Class<T> clazz);

    List<T> readFile(InputStream inputStream, int sheetNumber, Class<T> clazz);

    List<T> readFile(Sheet sheet, Class<T> clazz);

    Map<String, Integer> getHeaderMap(Row row);


}
