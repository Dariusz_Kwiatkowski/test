package tt.com.pl.test.excel.common.file;

import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import tt.com.pl.test.excel.common.FileReader;
import tt.com.pl.test.excel.common.SheetCell;

import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.*;

public abstract class ExcelFileReader<T> implements FileReader<T> {

    @SneakyThrows
    protected Workbook getWorkBook(String fileName) {
        FileInputStream file = new FileInputStream(fileName);
        return getWorkBook(file);
    }


    protected abstract Workbook getWorkBook(InputStream inputStream);


    @Override
    @SneakyThrows
    public List<T> readFile(String filePath, int sheetNumber, Class<T> clazz) {
        Sheet sheet = getSheet(filePath, sheetNumber);
        return readFile(sheet, clazz);
    }

    @Override
    @SneakyThrows
    public List<T> readFile(InputStream inputStream, int sheetNumber, Class<T> clazz) {
        Sheet sheet = getSheet(inputStream, sheetNumber);
        return readFile(sheet, clazz);
    }

    @Override
    @SneakyThrows
    public List<T> readFile(Sheet sheet, Class<T> clazz) {
        List<T> readObjects = new ArrayList<>();

        Iterator<Row> iterator = sheet.iterator();

        final Map<String, Integer> headerMap = getHeaderMap(iterator.next());

        while (iterator.hasNext()) {
            Row row = iterator.next();
            T obj = clazz.getDeclaredConstructor().newInstance();


            Arrays.stream(obj.getClass().getDeclaredFields())
                    .filter(x -> x.isAnnotationPresent(SheetCell.class))
                    .forEach(x -> setObjectValue(x, obj, getCellValue(row, x.getAnnotation(SheetCell.class).name(), headerMap)));

            readObjects.add(obj);
        }
        return readObjects;
    }


    private Object getCellValue(Row row, String annotation, final Map<String, Integer> headerMap) {
        return row.getCell(headerMap.get(annotation));
    }

    @Override
    public Map<String, Integer> getHeaderMap(Row row) {
        Map<String, Integer> headerMap = new HashMap<>();
        for (Cell cell : row) {
            int column = cell.getAddress().getColumn();
            headerMap.put(cell.toString(), column);
        }
        return headerMap;
    }


    //@TODO: map value (numeric, string, date etc.)
    @SneakyThrows
    private void setObjectValue(Field field, T obj, Object value) {
        field.setAccessible(true);
        field.set(obj, value.toString());
    }


    @SneakyThrows
    private Sheet getSheet(String filePath, int sheetNumber) {
        FileInputStream file = new FileInputStream(filePath);
        Workbook workbook = getWorkBook(file);
        return workbook.getSheetAt(sheetNumber);
    }

    @SneakyThrows
    private Sheet getSheet(InputStream inputStream, int sheetNumber) {
        Workbook workbook = getWorkBook(inputStream);
        return workbook.getSheetAt(sheetNumber);
    }
}
