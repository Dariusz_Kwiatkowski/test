package tt.com.pl.test.excel.common;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileInfo {
    private String filePath;
    private Integer sheetNumber;
}
